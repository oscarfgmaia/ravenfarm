import { Component, Input } from '@angular/core'
import { recipe } from '../types/recipe'
import { CommonModule, DecimalPipe } from '@angular/common'
import { item } from '../types/items'

@Component({
    selector: 'app-recipe-farming',
    standalone: true,
    imports: [DecimalPipe, CommonModule],
    templateUrl: './recipe-farming.component.html',
    styleUrl: './recipe-farming.component.scss',
})
export class RecipeComponent {
    @Input() recipe!: recipe
    @Input() index!: number
    @Input() service: any
    item!: item
    minPriceToSell!: number
    profit!: number
    constructor() {}
    ngOnInit() {
        this.service.recipe$.subscribe((recipes: recipe[]) => {
            recipes.forEach((recipe, index) => {
                if (index === this.index) {
                    this.minPriceToSell = recipe.price / recipe.haverstTimes / recipe.items[0].count[0]
                    this.profit =
                        recipe.items[0].marketPrice * recipe.items[0].count[0] - recipe.price / recipe.haverstTimes
                }
            })
        })
    }
}
