import { Component, inject } from '@angular/core'
import { recipe } from './types/recipe'
import { RecipeComponent } from './recipe-farming/recipe-farming.component'
import { FarmingService } from './farming.service'
import { MarketItemsComponent } from '../market-items/market-items.component'
import { CommonModule } from '@angular/common'
import { item } from './types/items'

@Component({
    selector: 'app-farming',
    standalone: true,
    imports: [RecipeComponent, MarketItemsComponent, CommonModule],
    templateUrl: './farming.component.html',
    styleUrl: './farming.component.scss',
})
export class FarmingComponent {
    service = inject(FarmingService)
    recipes: recipe[] = []
    items: item[] = []
    constructor() {
        this.service.recipe$.subscribe((recipes) => {
            this.recipes = recipes
        })
        this.service.items$.subscribe((items) => {
            this.items = items
            console.log(items)
        })
    }
    sortByMinValueToSell(): void {
        console.log('sortByMinValueToSell')

        let recipes = [...this.recipes].sort((a, b) => {
            const profitA = a.price / a.items[0].count[0] / a.haverstTimes
            const profitB = b.price / b.items[0].count[0] / b.haverstTimes
            return profitA - profitB
        })
        this.recipes = recipes
    }
    sortByProfit(): void {
        console.log('sortByProfit')
        let recipes = [...this.recipes].sort((a, b) => {
            const profitA = a.items[0].marketPrice * a.items[0].count[0] - a.price / a.haverstTimes
            const profitB = b.items[0].marketPrice * b.items[0].count[0] - b.price / b.haverstTimes
            return profitB - profitA
        })
        this.recipes = recipes
    }
}
