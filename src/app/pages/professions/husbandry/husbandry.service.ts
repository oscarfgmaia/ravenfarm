import { Injectable } from '@angular/core'
import { recipe } from './types/recipe'
import { BehaviorSubject, filter, map, tap } from 'rxjs'
import { item } from './types/items'

@Injectable({
    providedIn: 'root',
})
export class HusbandryService {
    private recipes: recipe[] = [
        {
            id: 34373,
            name: 'Small Chicken Pen',
            skillRequired: 5,
            experience: 0,
            price: 0,
            items: {
                gathering: [
                    {
                        id: 28928,
                        name: 'Egg',
                        marketPrice: 0,
                        count: [4, 6],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
                butchering: [
                    {
                        id: 28750,
                        name: 'Chicken Meat',
                        marketPrice: 0,
                        count: [3, 5],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
            },
            time: {
                butchering: 18000,
                gathering: 7200,
            },
        },
        {
            id: 34461,
            name: 'Small Hare Pen',
            skillRequired: 1,
            experience: 0,
            price: 0,
            items: {
                gathering: [
                    {
                        id: 34475,
                        name: 'Spool Of Hair',
                        marketPrice: 0,
                        count: [4, 6],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
                butchering: [
                    {
                        id: 28750,
                        name: 'Chicken Meat',
                        marketPrice: 0,
                        count: [3, 5],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
            },
            time: {
                butchering: 18000,
                gathering: 7200,
            },
        },
        {
            id: 34399,
            name: 'Small Pig Pen',
            skillRequired: 1,
            experience: 0,
            price: 0,
            items: {
                butchering: [
                    {
                        id: 28932,
                        name: 'Shank',
                        marketPrice: 0,
                        count: [3, 4],
                    },
                    {
                        id: 35353,
                        name: 'Hide',
                        marketPrice: 0,
                        count: [2, 4],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
            },
            time: {
                butchering: 28800,
            },
        },
        {
            id: 35083,
            name: 'Small Goat Pen',
            skillRequired: 10,
            experience: 0,
            price: 0,
            items: {
                gathering: [
                    {
                        id: 28929,
                        name: 'Milk',
                        marketPrice: 0,
                        count: [2, 3],
                    },
                    {
                        id: 28425,
                        name: 'Wool',
                        marketPrice: 0,
                        count: [1, 1],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
                butchering: [
                    {
                        id: 28932,
                        name: 'Shank',
                        marketPrice: 0,
                        count: [4, 7],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
            },
            time: {
                butchering: 23400,
                gathering: 9000,
            },
        },
        {
            id: 35085,
            name: 'Small Turkey Pen',
            skillRequired: 15,
            experience: 0,
            price: 0,
            items: {
                butchering: [
                    {
                        id: 28750,
                        name: 'Chicken Meat',
                        marketPrice: 0,
                        count: [3, 5],
                    },
                    {
                        id: 35354,
                        name: 'Feather',
                        marketPrice: 0,
                        count: [2, 5],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
            },
            time: {
                butchering: 34200,
            },
        },
        {
            id: 34412,
            name: 'Small Sheep Pen',
            skillRequired: 20,
            experience: 0,
            price: 0,
            items: {
                gathering: [
                    {
                        id: 28425,
                        name: 'Wool',
                        marketPrice: 0,
                        count: [3, 5],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
                butchering: [
                    {
                        id: 28932,
                        name: 'Shank',
                        marketPrice: 0,
                        count: [8, 12],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
            },
            time: {
                butchering: 43200,
                gathering: 18000,
            },
        },
        {
            id: 37440,
            name: 'Cheese Barrel',
            specialization: 'Fermenting Barrels',
            skillRequired: 21,
            experience: 0,
            price: 0,
            items: {
                gathering: [
                    {
                        id: 28927,
                        name: 'Cheese',
                        marketPrice: 0,
                        count: [1, 2],
                    },
                ],
            },
            time: {
                gathering: 108000,
            },
        },
        {
            id: 34386,
            name: 'Small Cow Pen',
            skillRequired: 25,
            experience: 0,
            price: 0,
            items: {
                gathering: [
                    {
                        id: 28929,
                        name: 'Milk',
                        marketPrice: 0,
                        count: [3, 5],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
                butchering: [
                    {
                        id: 28931,
                        name: 'Beef',
                        marketPrice: 0,
                        count: [6, 11],
                    },
                    {
                        id: 35353,
                        name: 'Hide',
                        marketPrice: 0,
                        count: [6, 9],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
            },
            time: {
                butchering: 72000,
                gathering: 10800,
            },
        },
        {
            id: 34377,
            name: 'Medium Chicken Pen',
            skillRequired: 30,
            experience: 0,
            price: 0,
            items: {
                gathering: [
                    {
                        id: 28928,
                        name: 'Egg',
                        marketPrice: 0,
                        count: [9, 14],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
                butchering: [
                    {
                        id: 28750,
                        name: 'Chicken Meat',
                        marketPrice: 0,
                        count: [7, 12],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
            },
            time: {
                butchering: 18000,
                gathering: 7200,
            },
        },
        {
            id: 34465,
            name: 'Medium Hare Pen',
            skillRequired: 35,
            experience: 0,
            price: 0,
            items: {
                gathering: [
                    {
                        id: 34475,
                        name: 'Spool Of Hair',
                        marketPrice: 0,
                        count: [9, 14],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
                butchering: [
                    {
                        id: 28750,
                        name: 'Chicken Meat',
                        marketPrice: 0,
                        count: [7, 12],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
            },
            time: {
                butchering: 18000,
                gathering: 7200,
            },
        },
        {
            id: 34403,
            name: 'Medium Pig Pen',
            skillRequired: 40,
            experience: 0,
            price: 0,
            items: {
                butchering: [
                    {
                        id: 28932,
                        name: 'Shank',
                        marketPrice: 0,
                        count: [6, 9],
                    },
                    {
                        id: 35353,
                        name: 'Hide',
                        marketPrice: 0,
                        count: [6, 9],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
            },
            time: {
                butchering: 28800,
            },
        },
        {
            id: 35082,
            name: 'Medium Goat Pen',
            skillRequired: 50,
            experience: 0,
            price: 0,
            items: {
                gathering: [
                    {
                        id: 28929,
                        name: 'Milk',
                        marketPrice: 0,
                        count: [4, 6],
                    },
                    {
                        id: 28425,
                        name: 'Wool',
                        marketPrice: 0,
                        count: [2, 4],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
                butchering: [
                    {
                        id: 28932,
                        name: 'Shank',
                        marketPrice: 0,
                        count: [9, 14],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
            },
            time: {
                butchering: 23400,
                gathering: 9000,
            },
        },
        {
            id: 35084,
            name: 'Medium Turkey Pen',
            skillRequired: 55,
            experience: 0,
            price: 0,
            items: {
                butchering: [
                    {
                        id: 28750,
                        name: 'Chicken Meat',
                        marketPrice: 0,
                        count: [9, 13],
                    },
                    {
                        id: 35354,
                        name: 'Feather',
                        marketPrice: 0,
                        count: [5, 8],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
            },
            time: {
                butchering: 34200,
            },
        },
        {
            id: 34416,
            name: 'Medium Sheep Pen',
            skillRequired: 60,
            experience: 0,
            price: 0,
            items: {
                gathering: [
                    {
                        id: 28425,
                        name: 'Wool',
                        marketPrice: 0,
                        count: [8, 11],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
                butchering: [
                    {
                        id: 28932,
                        name: 'Shank',
                        marketPrice: 0,
                        count: [18, 27],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
            },
            time: {
                butchering: 43200,
                gathering: 18000,
            },
        },
        {
            id: 34390,
            name: 'Medium Cow Pen',
            skillRequired: 65,
            experience: 0,
            price: 0,
            items: {
                gathering: [
                    {
                        id: 28929,
                        name: 'Milk',
                        marketPrice: 0,
                        count: [8, 11],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
                butchering: [
                    {
                        id: 28931,
                        name: 'Beef',
                        marketPrice: 0,
                        count: [15, 22],
                    },
                    {
                        id: 35353,
                        name: 'Hide',
                        marketPrice: 0,
                        count: [14, 21],
                    },
                    {
                        id: 34369,
                        name: 'Fertilizer',
                        marketPrice: 0,
                        count: [0, 1],
                    },
                ],
            },
            time: {
                butchering: 72000,
                gathering: 10800,
            },
        },
        {
            id: 44230,
            name: 'Bee Hive',
            specialization: 'Beekeeper',
            skillRequired: 21,
            experience: 0,
            price: 0,
            items: {
                gathering: [
                    {
                        id: 44381,
                        name: 'Honey',
                        marketPrice: 0,
                        count: [12, 24],
                    },
                ],
            },
            time: {
                gathering: 162000,
            },
        },
    ]
    public recipeSubject = new BehaviorSubject<recipe[]>(this.recipes)
    constructor() {}
    public recipe$ = this.recipeSubject.asObservable()
    public butcheringItems$ = this.recipe$.pipe(
        map((recipes) => {
            const allItems = recipes.map((recipe) => {
                return recipe.items.butchering
            })
            const filteredItems = allItems.filter((item) => item !== undefined)
            filteredItems.map((item) => {
                if (item!.length > 1) item!.pop()
            })
            const auxiliarArr: item[] = []
            filteredItems.forEach((item) => item!.forEach((i) => auxiliarArr.push(i)))
            const uniqueItems = auxiliarArr.filter((item, index, array) => {
                return array.findIndex((i) => i.name === item.name) === index
            })
            console.log('butchering', uniqueItems)
            return uniqueItems
        })
    )
    public gatheringItems$ = this.recipe$.pipe(
        map((recipes) => {
            const allItems = recipes.map((recipe) => {
                return recipe.items.gathering
            })
            const filteredItems = allItems.filter((item) => item !== undefined)
            filteredItems.map((item) => {
                if (item!.length > 1) item!.pop()
            })
            const auxiliarArr: item[] = []
            filteredItems.forEach((item) => item!.forEach((i) => auxiliarArr.push(i)))
            const uniqueItems = auxiliarArr.filter((item, index, array) => {
                return array.findIndex((i) => i.name === item.name) === index
            })
            console.log('gathering', uniqueItems)
            return uniqueItems
        })
    )

    updateAllMarketPrices() {
        // this.recipes = this.recipes.map((i) => {
        //     let itemPrice = localStorage.getItem(i.items[0].id.toString())
        //     if (itemPrice) {
        //         i.items[0].marketPrice = +itemPrice
        //     }
        //     return i
        // })
        // this.recipeSubject.next(this.recipes)
    }
}
