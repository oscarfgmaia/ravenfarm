import { item } from './items'

export type recipe = {
    name: string
    price: number
    id: number
    specialization?: string
    experience: number
    skillRequired: number
    time: { butchering?: number; gathering?: number }
    items: { gathering?: item[]; butchering?: item[] }
}
