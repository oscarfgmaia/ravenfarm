export type item = {
    name: string
    id: number
    marketPrice: number
    count: number[]
}
