import { Component, inject } from '@angular/core'
import { HusbandryService } from './husbandry.service'
import { recipe } from './types/recipe'
import { item } from './types/items'
import { MarketItemsComponent } from '../market-items/market-items.component'

@Component({
    selector: 'app-husbandry',
    standalone: true,
    imports: [MarketItemsComponent],
    templateUrl: './husbandry.component.html',
    styleUrl: './husbandry.component.scss',
})
export class HusbandryComponent {
    service = inject(HusbandryService)
    recipes: recipe[] = []
    butcheringItems: item[] = []
    gatheringItems: item[] = []
    constructor() {
        this.service.recipe$.subscribe((recipes) => {
            this.recipes = recipes
        })
        this.service.butcheringItems$.subscribe((items) => {
            this.butcheringItems = items
        })
        this.service.gatheringItems$.subscribe((items) => {
            this.gatheringItems = items
        })
    }
}
