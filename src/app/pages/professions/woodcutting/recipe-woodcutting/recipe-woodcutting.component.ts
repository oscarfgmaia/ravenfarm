import { Component, inject, Input } from '@angular/core'
import { recipe } from '../types/recipe'
import { item } from '../types/items'
import { CommonModule } from '@angular/common'
import { WoodcuttingService } from '../woodcutting.service'

@Component({
    selector: 'app-recipe-woodcutting',
    standalone: true,
    imports: [CommonModule],
    templateUrl: './recipe-woodcutting.component.html',
    styleUrl: './recipe-woodcutting.component.scss',
})
export class RecipeWoodcuttingComponent {
    @Input() recipe!: recipe
    @Input() index!: number
    item!: item
    minPriceToSell!: number
    profit!: number
    service = inject(WoodcuttingService)
    constructor() {}
    ngOnInit() {
        this.service.recipe$.subscribe((recipes: recipe[]) => {
            recipes.forEach((recipe, index) => {
                if (index === this.index) {
                    this.minPriceToSell = recipe.price / recipe.items[0].count[0]
                    this.profit = recipe.items[0].marketPrice * recipe.items[0].count[0] - recipe.price
                }
            })
        })
    }
}
