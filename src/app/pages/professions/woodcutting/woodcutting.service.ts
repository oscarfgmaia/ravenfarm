import { Injectable } from '@angular/core'
import { BehaviorSubject, filter, map } from 'rxjs'
import { recipe } from './types/recipe'

@Injectable({
    providedIn: 'root',
})
export class WoodcuttingService {
    constructor() {
        this.recipes.map((recipe) => {
            let itemPrice = localStorage.getItem(recipe.items[0].id.toString())
            if (itemPrice) {
                recipe.items[0].marketPrice = +itemPrice
            }
            return recipe
        })
    }
    private recipes: recipe[] = [
        {
            experience: 1600,
            price: 525,
            growingTime: 7200,
            skillRequired: 1,
            name: 'Juniper Tree',
            id: 34228,
            items: [
                {
                    name: 'Small Log',
                    marketPrice: 0,
                    id: 28977,
                    count: [4, 8],
                },
                {
                    name: 'Dense Log',
                    marketPrice: 0,

                    id: 34371,
                    count: [0, 1],
                },
            ],
        },
        {
            experience: 6400,
            price: 2100,
            growingTime: 28800,
            skillRequired: 5,
            name: 'Fir Tree',
            id: 34237,
            items: [
                {
                    name: 'Small Log',
                    marketPrice: 0,
                    id: 28977,
                    count: [20, 26],
                },
                {
                    name: 'Dense Log',
                    marketPrice: 0,
                    id: 34371,
                    count: [0, 1],
                },
            ],
        },
        {
            experience: 4800,
            price: 1575,
            growingTime: 21600,
            skillRequired: 10,
            name: 'Palm Tree',
            id: 34234,
            items: [
                {
                    name: 'Heavy Log',
                    marketPrice: 0,
                    id: 45504,
                    count: [14, 19],
                },
                {
                    name: 'Dense Log',
                    marketPrice: 0,
                    id: 34371,
                    count: [0, 1],
                },
            ],
        },
        {
            experience: 19000,
            price: 6300,
            growingTime: 86400,
            skillRequired: 20,
            name: 'Oak Tree',
            id: 34231,
            items: [
                {
                    name: 'Heavy Log',
                    marketPrice: 0,
                    id: 45504,
                    count: [50, 80],
                },
                {
                    name: 'Dense Log',
                    marketPrice: 0,
                    id: 34371,
                    count: [0, 1],
                },
            ],
        },
        {
            experience: 13000,
            price: 4200,
            growingTime: 57600,
            skillRequired: 35,
            name: 'Wildleaf Tree',
            id: 34225,
            items: [
                {
                    name: 'Sturdy Log',
                    marketPrice: 0,
                    id: 45505,
                    count: [28, 37],
                },
                {
                    name: 'Dense Log',
                    marketPrice: 0,
                    id: 34371,
                    count: [0, 1],
                },
            ],
        },
        {
            experience: 9600,
            price: 3150,
            growingTime: 43200,
            skillRequired: 50,
            name: 'Willow Tree',
            id: 34244,
            items: [
                {
                    name: 'Fine Log',
                    marketPrice: 0,
                    id: 45502,
                    count: [14, 19],
                },
                {
                    name: 'Dense Log',
                    marketPrice: 0,
                    id: 34371,
                    count: [0, 1],
                },
            ],
        },
    ]
    recipeSubject = new BehaviorSubject(this.recipes)

    public recipe$ = this.recipeSubject.asObservable()
    public items$ = this.recipe$.pipe(
        map((recipes) => {
            const allItems = recipes.map((recipe) => recipe.items[0])
            const uniqueItems = allItems.filter((item, index, array) => {
                return array.findIndex((i) => i.name === item.name) === index
            })
        return uniqueItems
        })
    )

    updateAllMarketPrices() {
        this.recipes = this.recipes.map((i) => {
            let itemPrice = localStorage.getItem(i.items[0].id.toString())
            if (itemPrice) {
                i.items[0].marketPrice = +itemPrice
            }
            return i
        })
        this.recipeSubject.next(this.recipes)
    }
}
