import { Component, inject } from '@angular/core'
import { MarketItemsComponent } from '../market-items/market-items.component'
import { recipe } from './types/recipe'
import { WoodcuttingService } from './woodcutting.service'
import { item } from './types/items'
import { RecipeWoodcuttingComponent } from './recipe-woodcutting/recipe-woodcutting.component'

@Component({
    selector: 'app-woodcutting',
    standalone: true,
    imports: [MarketItemsComponent, RecipeWoodcuttingComponent],
    templateUrl: './woodcutting.component.html',
    styleUrl: './woodcutting.component.scss',
})
export class WoodcuttingComponent {
    recipes: recipe[] = []
    items: item[] = []
    service = inject(WoodcuttingService)
    constructor() {
        this.service.recipe$.subscribe((recipes) => {
            this.recipes = recipes
        })
        this.service.items$.subscribe((items) => {
            this.items = items
        })
    }
    sortByMinValueToSell(): void {
        console.log('sortByMinValueToSell')

        let recipes = [...this.recipes].sort((a, b) => {
            const profitA = a.price / a.items[0].count[0]
            const profitB = b.price / b.items[0].count[0]
            return profitA - profitB
        })
        this.recipes = recipes
    }
    sortByProfit(): void {
        console.log('sortByProfit')
        let recipes = [...this.recipes].sort((a, b) => {
            const profitA = a.items[0].marketPrice * a.items[0].count[0] - a.price
            const profitB = b.items[0].marketPrice * b.items[0].count[0] - b.price
            return profitB - profitA
        })
        this.recipes = recipes
    }
}
