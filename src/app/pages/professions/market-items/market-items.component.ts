import { Component, Input } from '@angular/core'
import { CommonModule } from '@angular/common'
import { item } from '../farming/types/items'
import { recipe } from '../farming/types/recipe'

@Component({
    selector: 'app-market-items',
    standalone: true,
    imports: [CommonModule],
    templateUrl: './market-items.component.html',
    styleUrl: './market-items.component.scss',
})
export class MarketItemsComponent {
    @Input() items: item[] = []
    @Input() service: any
    constructor() {}
    updatePrice(e: any, itemId: number) {
        this.service.recipe$.subscribe((recipes: recipe[]) => {
            recipes.forEach((recipe) => {
                recipe.items.forEach((item) => {
                    if (item.id === itemId) {
                        item.marketPrice = e.target.value
                        localStorage.setItem(itemId.toString(), e.target.value)
                    }
                })
            })
        })
        this.service.updateAllMarketPrices()
    }
}
