import { Routes } from '@angular/router'
import { FarmingComponent } from './pages/professions/farming/farming.component'
import { HomeComponent } from './pages/home/home.component'
import { WoodcuttingComponent } from './pages/professions/woodcutting/woodcutting.component'
import { HusbandryComponent } from './pages/professions/husbandry/husbandry.component'

export const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'farming', component: FarmingComponent },
    { path: 'woodcutting', component: WoodcuttingComponent },
    { path: 'husbandry', component: HusbandryComponent },
]
